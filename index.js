// функция вызывается в глобальной области видимости сразу после загрузки страницы(как в анимастере)  и воззвращает объект с полями,
// объект деструктуризируется и его поля попадают в переменные showHorses showAccount и setWager
// я походу выкупил

// понять нужен ли ретерн в саме и почему?
let {
  showHorses,
  showAccount,
  setWager,
  showWagers,
  startRacing,
  newGame,
} = (function () {
  let horses = [];
  horses.push(new Horse(`horse1`));
  horses.push(new Horse(`horse2`));

  let player = new Player(100);

  let wagers = [];

  function Wager(cashValue, horseName) {
    this.CashValue = cashValue;
    this.horseName = horseName;
  }
  function Player(playerCash) {
    this.playerCash = playerCash;
    this.checkMoney = (cashToTake) => {
      return player.playerCash - cashToTake > 0 ? true : false;
    };
    this.getCash = () => {
      return this.playerCash;
    };
    this.setCash = (newCashValue) => {
      this.playerCash = newCashValue;
    };
  }
  function Horse(horseName) {
    this.horseName = horseName;
    this.horseResult = null;
    this.run = () => {
      return new Promise((resolve) => {
        let horseTime = getRandomInt(500, 3000);
        this.horseResult = horseTime;
        setTimeout(() => resolve(this), horseTime);
      });
    };
  }
  function getRandomInt(min, max) {
    let randomInt = 0;
    do {
      randomInt = Math.floor(Math.random() * max);
    } while (randomInt < min);
    return randomInt;
  }
  function giveWinCash(winCashValue) {
    let newCashValue = player.getCash() + winCashValue * 2;
    player.setCash(newCashValue);
    return winCashValue;
  }
  function outputRaceResults(allPromises, winCash) {
    Promise.all(allPromises).then(() => {
      console.log(
        `Вы выиграли ${winCash * 2}$! Баланс ${player.getCash()}$`
      );
    });
  }
  function calculateWinCash(allPromises) {
    return Promise.race(allPromises).then((winnerHorseInfo) => {
      let onlyWinWagers = wagers.filter((wager) => {
        return wager.horseName === winnerHorseInfo.horseName;
      });
      let reducer = (acc, currentWager) => acc + currentWager.CashValue;
      let sum = onlyWinWagers.reduce(reducer, 0);
      return sum;
    });
  }
  return {
    showHorses: function () {
      console.log("В забеге участвуют следующие лошади:\n");
      horses.forEach((horse) => {
        console.log(`${horse.horseName}\n`);
      });
    },
    showAccount: function () {
      console.log(`На вашем счету ${player.getCash()}$`);
    },
    setWager: function (cashValue, horseName) {
      if (player.checkMoney(cashValue)) {
        let newCashValue = player.getCash() - cashValue;
        player.setCash(newCashValue);
        wagers.push(new Wager(cashValue, horseName));
        console.log(`Ставка сделана!\n`);
      } else {
        console.error(`Ошибка! Недостаточно средств!`);
      }
    },
    showWagers: function () {
      wagers.forEach((wager) => {
        console.log(wager);
      });
    },
    startRacing: function () {
      let allPromises = horses.map((horse) => {
        return horse.run();
      });

      allPromises.forEach((horsePromise) => {
        horsePromise.then((finishedHorseInfo) => {
          console.log(
            `${finishedHorseInfo.horseName} - ${finishedHorseInfo.horseResult}`
          );
        });
      });

      calculateWinCash(allPromises)
        .then((winCashValue) => giveWinCash(winCashValue))
        .then((winCashValue) => {
          outputRaceResults(allPromises, winCashValue)
          wagers = [];
        }
        );
    },
    newGame: function () {
      player.setCash(100);
      wagers = [];
    },
  };
})();
